﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ppc.Startup))]
namespace Ppc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
