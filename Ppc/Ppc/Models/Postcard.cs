﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ppc.Models
{
	public class Postcard
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
	}

	//public class PostcardList : List<Postcard> { }
}