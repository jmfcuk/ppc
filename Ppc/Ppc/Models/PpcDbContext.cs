﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ppc.Models
{
	public class PpcDbContext : DbContext
	{
		public DbSet<Postcard> Postcards { get; set; }

		public PpcDbContext()
			: base("PortobelloPostcards")
		{
			
		}

		public static PpcDbContext Create()
		{
			return new PpcDbContext();
		}
	}
}