﻿#define _LOCAL 

using Ppc.Models;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ppc.Controllers
{
    public class PostcardsController : Controller
    {
        // GET: Postcards
        public ActionResult Index()
        {
			List<Postcard> pl = null;

#if _LOCAL

			pl = new List<Postcard>() {

				new Postcard() { Id = 1, Name = "postcard 1", Url = "/img/postcards/Castle.jpg" },
				new Postcard() { Id = 2, Name = "postcard 2", Url = "/img/postcards/Earl.jpg" },
				new Postcard() { Id = 3, Name = "postcard 3", Url = "/img/postcards/Gold.jpg" },
				new Postcard() { Id = 4, Name = "postcard 4", Url = "/img/postcards/GroundFloor.jpg" },
			};
#else
			pl = new List<Postcard>() {

				new Postcard() { Id = 1, Name = "postcard 1", Url = "/new/img/postcard-packs/1/Alice-Groundfloor-TIFF.jpg" },
				new Postcard() { Id = 1, Name = "postcard 2", Url = "/new/img/postcard-packs/1/Castle.jpg" },
				new Postcard() { Id = 1, Name = "postcard 3", Url = "/new/img/postcard-packs/1/Earl.jpg" },
				new Postcard() { Id = 1, Name = "postcard 4", Url = "/new/img/postcard-packs/1/Electric-Entrance - Tiff.jpg" },
				new Postcard() { Id = 1, Name = "postcard 5", Url = "/new/img/postcard-packs/1/Finchs-Mini-TIFF.jpg" },
				new Postcard() { Id = 1, Name = "postcard 6", Url = "/new/img/postcard-packs/1/Gold-Betsie-TIFF.jpg" },
				new Postcard() { Id = 1, Name = "postcard 7", Url = "/new/img/postcard-packs/1/Gold.jpg" },
				new Postcard() { Id = 1, Name = "postcard 8", Url = "/new/img/postcard-packs/1/GroundFloor.jpg" },
				new Postcard() { Id = 1, Name = "postcard 9", Url = "/new/img/postcard-packs/1/Jockey-Strongbow.jpg" },
				new Postcard() { Id = 1, Name = "postcard 10", Url = "/new/img/postcard-packs/1/Jockey_and-Buster-The-Sun-in-Splender (1).jpg" },
				new Postcard() { Id = 1, Name = "postcard 11", Url = "/new/img/postcard-packs/1/London-Bus.jpg" },
				new Postcard() { Id = 1, Name = "postcard 12", Url = "/new/img/postcard-packs/1/Portobello-fruit-and-veg-stall -TIFF.jpg" },
			};

#endif
			return View(pl);
        }
    }
}

