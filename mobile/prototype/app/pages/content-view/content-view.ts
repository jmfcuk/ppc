import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/content-view/content-view.html'
})
export class ContentViewPage {

  constructor(private _navController:NavController) {
  }

  //goToFactsPage(){
    //this._navController.push(ScientificFactsPage);
  //}
}
