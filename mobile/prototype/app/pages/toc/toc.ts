import {Component, OnInit} from "@angular/core";
import {NavController, NavParams} from 'ionic-angular';
import {StoriesService} from '../../services/stories';
import {ContentViewPage} from '../content-view/content-view';

@Component({
  templateUrl: 'build/pages/toc/toc.html',
  providers: [StoriesService]
})

export class TocPage implements OnInit {

  selectedItem: any;
  icons: string[];
  items: Array<{id: number, title: string, desc: string, icon: string}>;

  constructor(private nav: NavController, navParams: NavParams,
              private storiesSvc: StoriesService) {

    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];

  }

  itemTapped(event, item) {

    this.nav.push(ContentViewPage);

    // this.nav.push(TocPage, {
    //               item: item });
  }

  ngOnInit() {

    this.storiesSvc.getContents()
    .subscribe(
            data => {     

              for(let d of data.json()) {
	              this.items.push({
                  id:  d.id,
                  title: 'Item ' + d.id,
                  desc: d.desc,
                  icon: this.icons[Math.floor(Math.random() * this.icons.length)]

                });  
              }
            },
            err => console.error(err)
    );
  }
}
