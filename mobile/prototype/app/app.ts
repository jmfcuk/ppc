import {Component, OnInit, ViewChild} from '@angular/core';
import {App, ionicBootstrap, Platform, Nav} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {HomePage} from './pages/home/home';
import {TocPage} from './pages/toc/toc';
import {BookmarksPage} from './pages/bookmarks/bookmarks';

@Component({
  templateUrl: 'build/app.html'
})

class MyApp implements OnInit {

  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  pages: Array<{title: string, component: any}>

  constructor(private platform: Platform) {
   
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  ngOnInit() {

    this.initializeApp();

    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Contents', component: TocPage },
      { title: 'Bookmarks', component: BookmarksPage }
    ];
  }
}

ionicBootstrap(MyApp);


