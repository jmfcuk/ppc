import {Injectable} from '@angular/core';  
import {Http, Headers} from '@angular/http';

@Injectable()
export class StoriesService {  
    constructor(private http: Http) {
    }

    getContents() {

        var contents = this.http.get('http://localhost:56253/api/stories/contents');

        return contents;
    }
}

