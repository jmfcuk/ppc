﻿using PortobelloPostCardsCoUk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PortobelloPostCardsCoUk.Controllers.Api
{
	public class ProductsController : ApiController
	{
		private PortobelloPostcardsDbContext _db = new PortobelloPostcardsDbContext();

		// GET: api/ProductsApi
		public List<ProductGroup> GetProductGroups()
		{
			List<ProductGroup> pgs = null;

			pgs = _db.ProductGroups.Include("ProductGroups").ToList(); //.Include("Products").ToList();
			
			return pgs;
		}

		// GET: api/ProductsApi/5
		public List<Product> GetProducts(long? id)
		{
			List<Product> p = null;

			if(null != id)
				p = _db.Products.Where(d => d.ProductGroupId == id).ToList();

			return p;
		}
	}
}
