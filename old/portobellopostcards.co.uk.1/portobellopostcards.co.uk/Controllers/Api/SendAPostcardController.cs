﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PortobelloPostCardsCoUk.Models;

namespace PortobelloPostCardsCoUk.Controllers.Api
{
    public class SendAPostcardController : ApiController
    {
        private PortobelloPostcardsDbContext db = new PortobelloPostcardsDbContext();

        // GET: api/SendAPostcard
		//public IQueryable<SendAPostcard> GetSendAPostcard()
		//{
		//	return db.SendAPostcard;
		//}

		//// GET: api/SendAPostcard/5
		//[ResponseType(typeof(SendAPostcard))]
		//public IHttpActionResult GetSendAPostcard(long id)
		//{
		//	SendAPostcard sendAPostcard = db.SendAPostcard.Find(id);
		//	if (sendAPostcard == null)
		//	{
		//		return NotFound();
		//	}

		//	return Ok(sendAPostcard);
		//}

        // PUT: api/SendAPostcard/5
		//[ResponseType(typeof(void))]
		//public IHttpActionResult PutSendAPostcard(long id, SendAPostcard sendAPostcard)
		//{
		//	if (!ModelState.IsValid)
		//	{
		//		return BadRequest(ModelState);
		//	}

		//	if (id != sendAPostcard.Id)
		//	{
		//		return BadRequest();
		//	}

		//	db.Entry(sendAPostcard).State = EntityState.Modified;

		//	try
		//	{
		//		db.SaveChanges();
		//	}
		//	catch (DbUpdateConcurrencyException)
		//	{
		//		if (!SendAPostcardExists(id))
		//		{
		//			return NotFound();
		//		}
		//		else
		//		{
		//			throw;
		//		}
		//	}

		//	return StatusCode(HttpStatusCode.NoContent);
		//}

        // POST: api/SendAPostcard
        

		[ResponseType(typeof(SendAPostcard))]
        public IHttpActionResult Send(SendAPostcard sendAPostcard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

			sendAPostcard.TimeSent = DateTime.UtcNow;

            db.SendAPostcard.Add(sendAPostcard);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sendAPostcard.Id }, sendAPostcard);
        }

        // DELETE: api/SendAPostcard/5
		//[ResponseType(typeof(SendAPostcard))]
		//public IHttpActionResult DeleteSendAPostcard(long id)
		//{
		//	SendAPostcard sendAPostcard = db.SendAPostcard.Find(id);
		//	if (sendAPostcard == null)
		//	{
		//		return NotFound();
		//	}

		//	db.SendAPostcard.Remove(sendAPostcard);
		//	db.SaveChanges();

		//	return Ok(sendAPostcard);
		//}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SendAPostcardExists(long id)
        {
            return db.SendAPostcard.Count(e => e.Id == id) > 0;
        }
    }
}