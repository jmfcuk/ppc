﻿using PortobelloPostCardsCoUk.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace PortobelloPostCardsCoUk.Controllers.Api
{
    public class ContentController : Controller
    {
		private PortobelloPostcardsDbContext _db = new PortobelloPostcardsDbContext();

		public JsonResult Texts()
		{
			TextContentPlaceholderDictionary tcd =
				TextContentPlaceholderDictionary.FromList(_db.TextContent.ToList());

			JsonResult jr = new JsonResult();
			jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			jr.Data = tcd;

			return jr;
		}

		public JsonResult Text(string id)
		{
			TextContent tc = _db.TextContent.Where(t => t.Placeholder == id).FirstOrDefault();
			
			JsonResult jr = new JsonResult();
			jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			jr.Data = tc;

			return jr;
		}

		/* ********************************************************************************** */


		public JsonResult Images()
		{
			ImageContentPlaceholderDictionary icd =
				ImageContentPlaceholderDictionary.FromList(_db.ImageContent.ToList());

			JsonResult jr = new JsonResult();
			jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			jr.Data = icd;

			return jr;
		}

		public JsonResult Image(string id)
		{
			ImageContent ic = _db.ImageContent.Where(i => i.Placeholder == id).FirstOrDefault();

			JsonResult jr = new JsonResult();
			jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			jr.Data = ic;

			return jr;
		}
	}
}

