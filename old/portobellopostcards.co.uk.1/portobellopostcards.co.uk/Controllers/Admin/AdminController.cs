﻿using System.Web.Mvc;

namespace PortobelloPostCardsCoUk.Controllers.Admin
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller { }
}

