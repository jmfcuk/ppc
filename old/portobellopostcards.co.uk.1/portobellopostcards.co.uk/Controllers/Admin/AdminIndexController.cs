﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortobelloPostCardsCoUk.Controllers.Admin
{
    public class AdminIndexController : AdminController
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
    }
}

