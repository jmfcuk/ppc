﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PortobelloPostCardsCoUk.Models;
using PortobelloPostCardsCoUk.Controllers.Attributes;

namespace PortobelloPostCardsCoUk.Controllers.Admin
{
	//[SlugToProductGroupIdAttribute]
    public class AdminProductGroupsController : AdminController
    {
        private PortobelloPostcardsDbContext _db = new PortobelloPostcardsDbContext();

        // GET: ProductGroups
		public ActionResult Index(long? id)
        {
			List<ProductGroup> pgs  = null;

			if(null == id)
				pgs = _db.ProductGroups.ToList();
			else
				pgs = _db.ProductGroups.Where(c => c.ProductGroupId == id).ToList();

            return View(pgs);
        }

        // GET: ProductGroups/Details/5
		public ActionResult Details(long? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ProductGroup productGroup = _db.ProductGroups.Find(id);
			if (productGroup == null)
			{
				return HttpNotFound();
			}
			return View(productGroup);
		}

        // GET: ProductGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,ProductGroupIDName,Slug,Description")] ProductGroup productGroup)
        {
            if (ModelState.IsValid)
            {
				productGroup.UUid = new Guid();

                _db.ProductGroups.Add(productGroup);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productGroup);
        }

        // GET: ProductGroups/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductGroup productGroup = _db.ProductGroups.Find(id);
            if (productGroup == null)
            {
                return HttpNotFound();
            }
            return View(productGroup);
        }

        // POST: ProductGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Name,Slug,Description")] ProductGroup productGroup)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(productGroup).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productGroup);
        }

        // GET: ProductGroups/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductGroup productGroup = _db.ProductGroups.Find(id);
            if (productGroup == null)
            {
                return HttpNotFound();
            }
            return View(productGroup);
        }

        // POST: ProductGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ProductGroup productGroup = _db.ProductGroups.Find(id);
            _db.ProductGroups.Remove(productGroup);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
