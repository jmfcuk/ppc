﻿using PortobelloPostCardsCoUk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortobelloPostCardsCoUk.Controllers.Attributes
{
	public class SlugToProductItemsIdAttribute : SlugToIdAttribute
	{
		public override long SlugToId(string slug)
		{
			long result = BAD_ID;

			using (PortobelloPostcardsDbContext context = new PortobelloPostcardsDbContext())
			{
				var qry = from i in context.ProductItems where (0 == string.Compare(i.Slug, slug)) select i.Id;

				if (qry.Any())
					result = qry.First();
			}

			return result;
		}
	}
}
