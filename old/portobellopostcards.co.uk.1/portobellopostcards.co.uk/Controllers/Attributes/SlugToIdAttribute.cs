﻿using PortobelloPostCardsCoUk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortobelloPostCardsCoUk.Controllers.Attributes
{
	public abstract class SlugToIdAttribute : ActionFilterAttribute
	{
		private static IDictionary<string, long> Slugs = new Dictionary<string, long>();

		protected const long BAD_ID = -1L;

		public abstract long SlugToId(string slug);

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			string slug = filterContext.RouteData.Values["id"] as string;

			if (slug != null)
			{
				long id = BAD_ID;

				if (Slugs.TryGetValue(slug, out id))
				{
					if (0L < id)
						filterContext.ActionParameters["id"] = id;
				}
				else
				{
					long result = SlugToId(slug);

					if(BAD_ID != result)
					{
						Slugs.Add(slug, id);

						filterContext.ActionParameters["id"] = result;
					}
				}
			}
			base.OnActionExecuting(filterContext);
		}
	}
}
