

angular.module('contentApp', [])
	.controller("textContentController", function ($scope, $http) {

		$scope.models = {};
		$scope.models.data = {};

		$scope.models.data.text = {
			placeholder: "text-content1",
			content: "",
			getTextContent: function () {

				var url = "/Content/Text/" + $scope.models.data.text.placeholder;

				$http.get(url).
					success(function (data) {
						$scope.models.data.text.content = data.Text;
						$scope.models.data.text.dirty = false;
					});
			},
			dirty: true,
		};

		$scope.$watch("models.data.text.placeholder", function (oldVal, newVal) {

			if (newVal != "" && undefined != newVal)
				$scope.models.data.text.getTextContent();

			$scope.models.data.text.dirty = true;
		});
	}).controller("imageContentController", function ($scope, $http) {

		$scope.models = {};
		$scope.models.data = {};

		$scope.models.data.image = {
			placeholder: "find-the-blue-door-no-speech",
			content: "",
			getImageContent: function () {

				var url = "/Content/Image/" + $scope.models.data.image.placeholder;

				$http.get(url).
					success(function (data) {
						$scope.models.data.image.content = data.Url;
						$scope.models.data.image.dirty = false;
					});
			},
			dirty: true,
		};

		$scope.$watch("models.data.image.placeholder", function (oldVal, newVal) {

			if (newVal != "" && undefined != newVal)
				$scope.models.data.image.getImageContent();

			$scope.models.data.image.dirty = true;
		});
	});













  