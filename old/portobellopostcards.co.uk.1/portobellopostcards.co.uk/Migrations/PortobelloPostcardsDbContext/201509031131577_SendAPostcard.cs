namespace PortobelloPostCardsCoUk.Migrations.PortobelloPostcardsDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SendAPostcard : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SendAPostcard",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductItemId = c.Long(nullable: false),
                        To_House = c.String(),
                        To_Street1 = c.String(),
                        To_Street2 = c.String(),
                        To_City = c.String(),
                        To_County = c.String(),
                        To_District = c.String(),
                        To_PostalCode = c.String(),
                        Message = c.String(),
                        SenderEmail = c.String(),
                        TimeSent = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SendAPostcard");
        }
    }
}
