namespace PortobelloPostCardsCoUk.Migrations.PortobelloPostcardsDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendAPostcard", "ToName", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_House", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_Street1", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_Street2", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_City", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_County", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_District", c => c.String());
            AddColumn("dbo.SendAPostcard", "ToAddress_PostalCode", c => c.String());
            DropColumn("dbo.SendAPostcard", "To_House");
            DropColumn("dbo.SendAPostcard", "To_Street1");
            DropColumn("dbo.SendAPostcard", "To_Street2");
            DropColumn("dbo.SendAPostcard", "To_City");
            DropColumn("dbo.SendAPostcard", "To_County");
            DropColumn("dbo.SendAPostcard", "To_District");
            DropColumn("dbo.SendAPostcard", "To_PostalCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SendAPostcard", "To_PostalCode", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_District", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_County", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_City", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_Street2", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_Street1", c => c.String());
            AddColumn("dbo.SendAPostcard", "To_House", c => c.String());
            DropColumn("dbo.SendAPostcard", "ToAddress_PostalCode");
            DropColumn("dbo.SendAPostcard", "ToAddress_District");
            DropColumn("dbo.SendAPostcard", "ToAddress_County");
            DropColumn("dbo.SendAPostcard", "ToAddress_City");
            DropColumn("dbo.SendAPostcard", "ToAddress_Street2");
            DropColumn("dbo.SendAPostcard", "ToAddress_Street1");
            DropColumn("dbo.SendAPostcard", "ToAddress_House");
            DropColumn("dbo.SendAPostcard", "ToName");
        }
    }
}
