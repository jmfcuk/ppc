﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PortobelloPostCardsCoUk.Models
{
	public class PortobelloPostcardsDbContext : DbContext
	{
		public PortobelloPostcardsDbContext()
			: base("PortobelloPostcards") { }

		public DbSet<ProductGroup> ProductGroups { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<ProductItem> ProductItems { get; set; }

		public DbSet<TextContent> TextContent { get; set; }
		public DbSet<ImageContent> ImageContent { get; set; }

		public DbSet<SendAPostcard> SendAPostcard { get; set; }

		public DbSet<PortobelloPostCardsCoUk.Models.SlugRoute> SlugRoutes { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			base.OnModelCreating(modelBuilder);
		}
	}
}

