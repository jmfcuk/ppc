﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortobelloPostCardsCoUk.Models
{
	public class SendAPostcard
	{
		public long Id { get; set; }

		public long ProductItemId { get; set; }

		//public ApplicationUser User { get; set; }

		public string ToName { get; set; }

		public UserAddress ToAddress { get; set; }

		public string Message { get; set; }

		public string SenderEmail { get; set; }

		public DateTime TimeSent { get; set; }
	}
}

