﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;

namespace PortobelloPostCardsCoUk.Models
{
	public class Content
	{
		public long Id { get; set; }

		public string Description { get; set; }

		public string Placeholder { get; set; }

		public string MimeType { get; set; }
	}

	public class TextContent : Content
	{
		public string Text { get; set; }
	}

	public class ImageContent : Content
	{
		public string Url { get; set; }
	}

	public class TextContentPlaceholderDictionary : Dictionary<string, TextContent> 
	{
		public static TextContentPlaceholderDictionary FromList(List<TextContent> list)
		{
			TextContentPlaceholderDictionary tcd = new TextContentPlaceholderDictionary();

			if (null != list)
			{
				foreach (TextContent tc in list)
					tcd.Add(tc.Placeholder, tc);
			}

			return tcd;
		}
	}

	public class ImageContentPlaceholderDictionary : Dictionary<string, ImageContent>
	{
		public static ImageContentPlaceholderDictionary FromList(List<ImageContent> list)
		{
			ImageContentPlaceholderDictionary icd = new ImageContentPlaceholderDictionary();

			if (null != list)
			{
				foreach (ImageContent ic in list)
					icd.Add(ic.Placeholder, ic);
			}

			return icd;
		}
	}
}

