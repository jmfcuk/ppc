﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PortobelloPostCardsCoUk.Startup))]
namespace PortobelloPostCardsCoUk
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
