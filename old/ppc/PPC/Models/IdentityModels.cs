﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Net;

namespace WebApp.Models
{
	public class UserAddress
	{
		public string House { get; set; }
		public string Street1 { get; set; }
		public string Street2 { get; set; }
		public string City { get; set; }
		public string County { get; set; }
		public string District { get; set; }
		public string PostalCode { get; set; }
	}

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime? Created { get; set; }

		public DateTime? LastLogin { get; set; }

		public string LastLoginIp { get; set; }

		public TimeSpan? LastLoginDuration { get; set; }

		public UserAddress Address { get; set; }

		public ApplicationUser()
			: base()
		{
			Address = new UserAddress();
		}

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            return userIdentity;
        }
    }

    public class PortobelloPostcardsIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public PortobelloPostcardsIdentityDbContext()
			: base("PortobelloPostcards", throwIfV1Schema: false)
        {
        }

        public static PortobelloPostcardsIdentityDbContext Create()
        {
            return new PortobelloPostcardsIdentityDbContext();
        }
    }
}

