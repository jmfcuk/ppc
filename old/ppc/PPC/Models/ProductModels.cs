﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ProductModel
    {
		public long Id { get; set; }

		public Guid UUid { get; set; }

        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }

		public long? ImageId { get; set; }
    }
	
    public class ProductGroup : ProductModel
    {
		public long? ProductGroupId { get; set; }

		public decimal? Price { get; set; }

		public virtual ICollection<ProductGroup> ProductGroups { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }

    public class Product : ProductModel
    {
        public long ProductGroupId { get; set; }

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
    
    public class ProductItem : ProductModel
    {
        public string Sku { get; set; }

		public string Brand { get; set; }

        public long ProductId { get; set; }

        public float? Size { get; set; }
        public long? Width { get; set; }
		public long? Height { get; set; }
		public long? Depth { get; set; }

        public string Colour { get; set; }

        public decimal? Price { get; set; }
    }

	public class ProductOrder
	{
		public long Id { get; set; }

		//public ApplicationUser User { get; set; }

		public string ToName { get; set; }

		public UserAddress ToAddress { get; set; }

		public string SenderEmail { get; set; }

		ICollection<ProductOrderLine> OrderLines { get; set; }
	}

	public class ProductOrderLine
	{
		public long Id { get; set; }

		public long ProductOrderId { get; set; }

		public long ProductItemId { get; set; }

		public int Quantity { get; set; }
	}
}

