﻿using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
	public class SlugRoute
	{
		public long Id { get; set; }

		public string Slug { get; set; }

		public string Controller { get; set; }

		public string Action { get; set; }
	}
}

