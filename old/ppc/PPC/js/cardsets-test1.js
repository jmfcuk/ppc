﻿
/* global CardSets */

var prefix = CardSets.prefix

var transform = prefix('transform')

var translate = CardSets.translate

var $container = document.getElementById('container1')

var deck = CardSets()

deck.cards.forEach(function (card, i) {

	card.enableDragging()
	card.enableFlipping()

	card.$el.addEventListener('mousedown', onTouch)
	card.$el.addEventListener('touchstart', onTouch)

	function onTouch() {

	}
})

function startWinning() {
	var $winningCardSets = document.createElement('div')
	$winningCardSets.classList.add('deck')

	$winningCardSets.style[transform] = translate(Math.random() * window.innerWidth - window.innerWidth / 2 + 'px', Math.random() * window.innerHeight - window.innerHeight / 2 + 'px')

	$container.appendChild($winningCardSets)

	var side = Math.floor(Math.random() * 2) ? 'front' : 'back'

	for (var i = 0; i < 55; i++) {
		addWinningCard($winningCardSets, i, side)
	}

	setTimeout(startWinning, Math.round(Math.random() * 1000))
}

function addWinningCard($deck, i, side) {
	var card = CardSets.Card(54 - i)
	var delay = (55 - i) * 20
	var animationFrames = CardSets.animationFrames
	var ease = CardSets.ease

	card.enableFlipping()

	if (side === 'front') {
		card.setSide('front')
	} else {
		card.setSide('back')
	}

	card.mount($deck)
	card.$el.style.display = 'none'

	var xStart = 0
	var yStart = 0
	var xDiff = -500
	var yDiff = 500

	animationFrames(delay, 1000)
	  .start(function () {
	  	card.x = 0
	  	card.y = 0
	  	card.$el.style.display = ''
	  })
	  .progress(function (t) {
	  	var tx = t
	  	var ty = ease.cubicIn(t)
	  	card.x = xStart + xDiff * tx
	  	card.y = yStart + yDiff * ty
	  	card.$el.style[transform] = translate(card.x + 'px', card.y + 'px')
	  })
	  .end(function () {
	  	card.unmount()
	  })
}


//$poker.addEventListener('click', function () {
//  deck.queue(function (next) {
//    deck.cards.forEach(function (card, i) {
//      setTimeout(function () {
//        card.setSide('back')
//      }, i * 7.5)
//    })
//    next()
//  })
//  deck.shuffle()
//  deck.shuffle()
//  deck.poker()
//})

deck.mount($container);

deck.intro();
deck.byset();


//deck.cards.forEach(function (card, i) {
//	card.fan();
//});

