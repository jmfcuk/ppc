
/* global CardSet */

var prefix = CardSet.prefix

var transform = prefix('transform')

var translate = CardSet.translate

var $container = document.getElementById('container')
var $topbar = document.getElementById('topbar')

//var $sort = document.createElement('button')
var $shuffle = document.createElement('button')
var $byset = document.createElement('button')
var $fan = document.createElement('button')
//var $poker = document.createElement('button')
var $flip = document.createElement('button')

$shuffle.textContent = 'Shuffle'
//$sort.textContent = 'Sort'
$byset.textContent = 'By set'
$fan.textContent = 'Fan'
//$poker.textContent = 'Poker'
$flip.textContent = 'Flip'

$topbar.appendChild($flip)
$topbar.appendChild($shuffle)
$topbar.appendChild($byset)
$topbar.appendChild($fan)
//$topbar.appendChild($poker)
//$topbar.appendChild($sort)

var deck = CardSet()


deck.cards.forEach(function (card, i) {

	card.enableDragging()
	card.enableFlipping()

	card.$el.addEventListener('mousedown', onTouch)
	card.$el.addEventListener('touchstart', onTouch)

	function onTouch() {

	}
})

function startWinning () {
  var $winningCardSet = document.createElement('div')
  $winningCardSet.classList.add('deck')

  $winningCardSet.style[transform] = translate(Math.random() * window.innerWidth - window.innerWidth / 2 + 'px', Math.random() * window.innerHeight - window.innerHeight / 2 + 'px')

  $container.appendChild($winningCardSet)

  var side = Math.floor(Math.random() * 2) ? 'front' : 'back'

  for (var i = 0; i < 55; i++) {
    addWinningCard($winningCardSet, i, side)
  }

  setTimeout(startWinning, Math.round(Math.random() * 1000))
}

function addWinningCard ($deck, i, side) {
  var card = CardSet.Card(54 - i)
  var delay = (55 - i) * 20
  var animationFrames = CardSet.animationFrames
  var ease = CardSet.ease

  card.enableFlipping()

  if (side === 'front') {
    card.setSide('front')
  } else {
    card.setSide('back')
  }

  card.mount($deck)
  card.$el.style.display = 'none'

  var xStart = 0
  var yStart = 0
  var xDiff = -500
  var yDiff = 500

  animationFrames(delay, 1000)
    .start(function () {
      card.x = 0
      card.y = 0
      card.$el.style.display = ''
    })
    .progress(function (t) {
      var tx = t
      var ty = ease.cubicIn(t)
      card.x = xStart + xDiff * tx
      card.y = yStart + yDiff * ty
      card.$el.style[transform] = translate(card.x + 'px', card.y + 'px')
    })
    .end(function () {
      card.unmount()
    })
}

// easter eggs end

$shuffle.addEventListener('click', function () {
  deck.shuffle()
  deck.shuffle()
})
//$sort.addEventListener('click', function () {
//  deck.sort()
//})
$byset.addEventListener('click', function () {
  deck.sort(true) // sort reversed
  deck.byset()
})
$fan.addEventListener('click', function () {
  deck.fan()
})
$flip.addEventListener('click', function () {
  deck.flip()
})
//$poker.addEventListener('click', function () {
//  deck.queue(function (next) {
//    deck.cards.forEach(function (card, i) {
//      setTimeout(function () {
//        card.setSide('back')
//      }, i * 7.5)
//    })
//    next()
//  })
//  deck.shuffle()
//  deck.shuffle()
//  deck.poker()
//})

deck.mount($container)

deck.intro()
deck.byset()
//deck.cards.forEach(function (card, i) {
//	card.fan();
//});

