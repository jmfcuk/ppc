﻿
function unloadPcPopupBox() {

	$('#postcard-popup').fadeOut("slow");

	$("#postcard-popup-cinner").empty();

	$("#postcard-popup-carousel").removeClass("slide");

	$("#body-container").css({
		"opacity": "1"
	});
}

function loadPcPopupBox(id) {

	$.getJSON("/api/Products/PackUrls/" + id, function (data, status) {

		var activeItem = $("<div>" +
					   "</div>").addClass("item active");

		var activeImg = $("<img class=\"\" src=\"\" alt=\"\" />").addClass("img-responsive")
			.attr("src", data[0]).attr("alt", "...").css("width","100%").css("height","100%");

		activeItem.append(activeImg);

		$("#postcard-popup-cinner").append(activeItem);

		//var activeInd = $("<li data-target=\"#postcard-popup-carousel\" data-slide-to=\"0\" class=\"active\"></li>");

		//$("#postcard-popup-carousel-indicators").append(activeInd);

		for (var i = 1; i < data.length; i++) {
			var item = $("<div>" +
					   "</div>").addClass("item col-md-12");

			var img = $("<img class=\"\" src=\"\" alt=\"\" />").addClass("img-responsive")
				.attr("src", data[i]).attr("alt", "...").css("width", "100%").css("height", "100%");

			item.append(img);

			$("#postcard-popup-cinner").append(item);

			//var ind = $("<li data-target=\"#postcard-popup-carousel\" data-slide-to=\"" + i.toString() + "\" class=\"active\"></li>");

			//$("#postcard-popup-carousel-indicators").append(ind);
		}

		var titleId = id + "-title";

		var title = $("#" + titleId).text();

		var hdr = $("#postcard-popup-header");

		hdr.text(title);

		$("#postcard-popup").draggable().resizable();

		$('#postcard-popup').fadeIn("slow");

		$("#body-container").css({
			"opacity": "0.3"
		});

		//$("#postcard-popup-carousel").addClass("slide");

		//$("#postcard-popup-carousel").carousel({
		//	pause: true,
		//	interval: false
		//});
	});
}