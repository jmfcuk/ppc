﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers.Admin;
using WebApp.Models;

namespace WebApp.Controllers
{
	//[Authorize(Roles = "Administrator")]
	public class CheckoutController : Controller
    {
        private PortobelloPostcardsDbContext db = new PortobelloPostcardsDbContext();

        // GET: Checkout
		//public ActionResult Index()
		//{
		//	return View(db.ProductOrders.ToList());
		//}

        // GET: Checkout/Details/5
		//public ActionResult Details(long? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	ProductOrder productOrder = db.ProductOrders.Find(id);
		//	if (productOrder == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(productOrder);
		//}

        // GET: Checkout/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Checkout/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ToName,ToAddress,SenderEmail,OrderLines")] ProductOrder productOrder)
        {
            if (ModelState.IsValid)
            {
                db.ProductOrders.Add(productOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productOrder);
        }

        // GET: Checkout/Edit/5
		//public ActionResult Edit(long? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	ProductOrder productOrder = db.ProductOrders.Find(id);
		//	if (productOrder == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(productOrder);
		//}

		//// POST: Checkout/Edit/5
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit([Bind(Include = "Id,ToName,ToAddress,SenderEmail")] ProductOrder productOrder)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		db.Entry(productOrder).State = EntityState.Modified;
		//		db.SaveChanges();
		//		return RedirectToAction("Index");
		//	}
		//	return View(productOrder);
		//}

		//// GET: Checkout/Delete/5
		//public ActionResult Delete(long? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	ProductOrder productOrder = db.ProductOrders.Find(id);
		//	if (productOrder == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(productOrder);
		//}

		//// POST: Checkout/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed(long id)
		//{
		//	ProductOrder productOrder = db.ProductOrders.Find(id);
		//	db.ProductOrders.Remove(productOrder);
		//	db.SaveChanges();
		//	return RedirectToAction("Index");
		//}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
