﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp.Models;

namespace WebApp.Controllers
{
	//[Authorize(Roles = "Administrator")]
	public class ProductsController : ApiController
	{
		private const short __PRODUCT_GROUP_POSTCARD_PACKS = 1;

		private PortobelloPostcardsDbContext _db = new PortobelloPostcardsDbContext();

		// GET: api/ProductsApi
		public List<ProductGroup> GetProductGroups()
		{
			List<ProductGroup> pgs = null;

			pgs = _db.ProductGroups.Include("ProductGroups").ToList();

			return pgs;
		}

		// GET: api/ProductsApi/5
		public List<Product> GetProducts(long? id)
		{
			List<Product> p = null;

			if (null != id)
				p = _db.Products.Where(d => d.ProductGroupId == id).ToList();

			return p;
		}

		public ContentCollection GetAllPostcardPacks()
		{
			ContentCollection cc = new ContentCollection();

			var pgroup = _db.ProductGroups.Where(pg => pg.Id == __PRODUCT_GROUP_POSTCARD_PACKS).First();

			cc.ProductGroup = pgroup;

			return cc;
		}

		public ContentCollection GetPostcardPack(string id)
		{
			//string pcpName = "";

			//switch(id)
			//{
			//	case "pcp1" :
			//		pcpName = "PostcardPack1";
			//		break;

			//	case "pcp2":
			//		pcpName = "PostcardsPack2";
			//		break;

			//	case "pcp3":
			//		pcpName = "PostcardPack3";
			//		break;

			//	default: break;
			//}

			//var prod = _db.Products.Where(p => p.Name == pcpName).FirstOrDefault();

			//var pcs = _db.ProductItems.Where(pi => pi.ProductId == prod.Id);

			//if (null == id)
			//	id = "7";

			//long lid = long.Parse(id);

			//var pgs = _db.Products.Where(pg => pg.Id == lid).First();

			//foreach (ProductItem prodItem in pgs.ProductItems)
			//{
			//	prodItem.Slug = "~/images/packs/" + pgs.Name + "/" + prodItem.Name;
			//}

			//return View(pgs);

			return null;

		}

		[HttpGet]
		public List<string> PackUrls(string id)
		{
			List<String> l = new List<string>();

			string pname = "PostcardPack" + id.TrimStart('p', 'c');

			string url = "/images/product/postcard-packs/" + pname;

			var pis = _db.Products.Where(p => p.Name == pname).Select(pi => pi.ProductItems);
			
			foreach(var p in pis.First())
			{
				l.Add(url + '/' + p.Name);
			}

			return l;
		}

	}
}


