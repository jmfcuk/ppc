﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApp.Controllers.Api
{
	public class Content
	{
		public int Id { get; set; }
		public string Desc { get; set; }
	}

	public class ContentList : List<Content> { }

	[EnableCors(origins: "*", headers: "*", methods: "GET, POST")]
	public class StoriesController : ApiController
	{
		[HttpGet]
		public ContentList Contents()
		{
			ContentList cl = new ContentList();

			for(int x = 0; x < 10; x++)
			{
				cl.Add(new Content()
				{
					Id = x + 1,
					Desc = $"Content {x + 1}",
				});
			}

			return cl;
		}
		
		[HttpGet]
		public object Story(int id)
		{
			return new
			{
				Id = id,
				Text = "Test",
			};
		} 
	}
}


