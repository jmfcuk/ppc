﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers.Admin;
using WebApp.Models;

namespace WebApp.Controllers
{
	//[Authorize(Roles = "Administrator")]
	public class HomeController : Controller
	{
		private PortobelloPostcardsDbContext _db = new PortobelloPostcardsDbContext();

		//private ContentCollection FixupUrls(ContentCollection cc)
		//{
		//	foreach (var tc in cc.Text.Values)
		//	{
		//		if (false == tc.Text.StartsWith("/"))
		//			tc.Text = "/" + tc.Text;
		//	}

		//	return cc;
		//}

		private string ReplaceNewlines(string s)
		{
			return s.Replace(Environment.NewLine, "<br />");
		}

		private ContentCollection GetContent(string group)
		{
			var tcl = _db.TextContent.Where(t => t.Group == group);
			var icl = _db.ImageContent.Where(t => t.Group == group);

			ContentCollection cc = new ContentCollection();

			foreach (TextContent tc in tcl)
			{
				tc.Text = ReplaceNewlines(tc.Text);

				cc.Text.Add(tc.Placeholder, tc);
			}

			foreach (ImageContent ic in icl)
				cc.Images.Add(ic.Placeholder, ic);

			return cc;
		}

		public ActionResult Index()
		{
			ContentCollection cc = GetContent("Index");
						
			return View(cc);
		}

		public ActionResult OurPostcards()
		{
			ContentCollection cc = GetContent("OurPostcards");

			return View(cc);
		}

		public ActionResult Prints()
		{
			ContentCollection cc = GetContent("Prints");

			return View("Template", cc);
		}

		public ActionResult BlackAndWhite()
		{
			ContentCollection cc = GetContent("BlackAndWhite");

			return View("Template", cc);
		}

		public ActionResult SendAPostcard()
		{
			ContentCollection cc = GetContent("SendAPostcard");

			return View("Template", cc);
		}

		public ActionResult Test()
		{
			return View();
		}
	}
}

 