﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
using System.IO;

namespace WebApp.Controllers.Admin
{
	public class AdminImageContentsController : AdminController
    {
		private const string _IMG_DIR = "~/images";
		private const string _UPLOAD_DIR = "~/App_Data/uploads";

        private PortobelloPostcardsDbContext db = new PortobelloPostcardsDbContext();

        // GET: ImageContents
        public ActionResult Index()
        {
            return View(db.ImageContent.ToList());
        }

        // GET: ImageContents/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageContent imageContent = db.ImageContent.Find(id);
            if (imageContent == null)
            {
                return HttpNotFound();
            }
            return View(imageContent);
        }

		private string MoveUploadImage(HttpPostedFileBase file)
		{
			string url = string.Empty;

			string fileName = Path.GetFileName(file.FileName);
			string path = Path.Combine(Server.MapPath(_UPLOAD_DIR), fileName);

			file.SaveAs(path);

			string newPath = Path.Combine(Server.MapPath(_IMG_DIR), Path.GetFileName(path));

			System.IO.File.Delete(newPath);

			System.IO.File.Move(path, newPath);

			url = Path.Combine(_IMG_DIR, fileName).Replace('\\', '/');

			return url;
		}

		[HttpPost]
		public ActionResult Upload([Bind(Include = "Id,Url,Description,Placeholder,MimeType")]ImageContent imageContent, HttpPostedFileBase file)
		{
			if (null != imageContent && null != file)
			{
				if (ModelState.IsValid)
				{
					imageContent.MimeType = file.ContentType;

					imageContent.Url = MoveUploadImage(file).TrimStart(new char[] { '~', '/', });

					ImageContent ic = db.ImageContent.Where(t => t.Id == imageContent.Id).FirstOrDefault();

					if (null == ic)
						db.ImageContent.Add(imageContent);
					else
					{
						ic.Url = imageContent.Url;
						ic.Description = imageContent.Description;
						ic.MimeType = imageContent.MimeType;
						ic.Placeholder = imageContent.Placeholder;

						db.Entry(ic).State = EntityState.Modified;
					}

					db.SaveChanges();
				}
			}

			return RedirectToAction("Index");
		}






        // GET: ImageContents/Create
        public ActionResult Create()
        {
            return View();
        }

		////////// POST: ImageContents/Create
		////////// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		////////// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		////////[HttpPost]
		////////[ValidateAntiForgeryToken]
		////////public ActionResult Create([Bind(Include = "Id,Url,Description,Placeholder,MimeType")] ImageContent imageContent)
		////////{
		////////	if (ModelState.IsValid)
		////////	{
		////////		db.ImageContent.Add(imageContent);
		////////		db.SaveChanges();
		////////		return RedirectToAction("Index");
		////////	}

		////////	return View(imageContent);
		////////}

        // GET: ImageContents/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageContent imageContent = db.ImageContent.Find(id);
            if (imageContent == null)
            {
                return HttpNotFound();
            }
            return View(imageContent);
        }

		////////// POST: ImageContents/Edit/5
		////////// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		////////// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		////////[HttpPost]
		////////[ValidateAntiForgeryToken]
		////////public ActionResult Edit([Bind(Include = "Id,Url,Description,Placeholder,MimeType")] ImageContent imageContent)
		////////{
		////////	if (ModelState.IsValid)
		////////	{
		////////		db.Entry(imageContent).State = EntityState.Modified;
		////////		db.SaveChanges();
		////////		return RedirectToAction("Index");
		////////	}
		////////	return View(imageContent);
		////////}

        // GET: ImageContents/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageContent imageContent = db.ImageContent.Find(id);
            if (imageContent == null)
            {
                return HttpNotFound();
            }
            return View(imageContent);
        }

        // POST: ImageContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ImageContent imageContent = db.ImageContent.Find(id);
            db.ImageContent.Remove(imageContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
