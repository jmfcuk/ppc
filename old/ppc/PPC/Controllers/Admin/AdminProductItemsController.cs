﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
//using WebApp.Controllers.Attributes;

namespace WebApp.Controllers.Admin
{
	//[SlugToProductItemsIdAttribute]
    public class AdminProductItemsController : AdminController
    {
        private PortobelloPostcardsDbContext db = new PortobelloPostcardsDbContext();

        // GET: AdminProductItems
        public ActionResult Index(long? id)
        {
            if (null == id)
                return View(db.ProductItems.ToList());
            else
            {
                ViewData["id"] = id;
                var items = db.ProductItems.Where(p => p.ProductId == id);

                return View(items.ToList());
            }
        }

        // GET: AdminProductItems/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductItem productItem = db.ProductItems.Find(id);
            if (productItem == null)
            {
                return HttpNotFound();
            }
            return View(productItem);
        }

        // GET: AdminProductItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminProductItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,ProductId,Sku,Size,Length,Width,Height,Colour,Price1,Price5,Price10,Price100,Price500,Price1000,Name,Slug,Description")] ProductItem productItem)
        {
            if (ModelState.IsValid)
            {
				productItem.UUid = new Guid();

                db.ProductItems.Add(productItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productItem);
        }

        // GET: AdminProductItems/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductItem productItem = db.ProductItems.Find(id);
            if (productItem == null)
            {
                return HttpNotFound();
            }
            return View(productItem);
        }

        // POST: AdminProductItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,ProductId,Sku,Size,Length,Width,Height,Colour,Price1,Price5,Price10,Price100,Price500,Price1000,Name,Slug,Description")] ProductItem productItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productItem);
        }

        // GET: AdminProductItems/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductItem productItem = db.ProductItems.Find(id);
            if (productItem == null)
            {
                return HttpNotFound();
            }
            return View(productItem);
        }

        // POST: AdminProductItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ProductItem productItem = db.ProductItems.Find(id);
            db.ProductItems.Remove(productItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
