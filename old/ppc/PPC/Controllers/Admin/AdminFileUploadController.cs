﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers.Admin
{
	public class AdminFileUploadController : Controller
    {
		private void ConvertFile(string path)
		{
			byte[] bytes = System.IO.File.ReadAllBytes(path);
		}

        // GET: AdminUploadFile
        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		public ActionResult Index(HttpPostedFileBase file)
		{
			if (file != null && file.ContentLength > 0)
				try
				{
					string path = Path.Combine(Server.MapPath("~/App_Data/uploads"),
											   Path.GetFileName(file.FileName));
					file.SaveAs(path);

					ConvertFile(path);

					ViewBag.Message = "File uploaded successfully";
				}
				catch (Exception ex)
				{
					ViewBag.Message = "ERROR:" + ex.Message.ToString();
				}
			else
			{
				ViewBag.Message = "You have not specified a file.";
			}
			return View();
		}
	}
}

