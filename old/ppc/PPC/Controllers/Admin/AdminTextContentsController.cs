﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
using System.IO;

namespace WebApp.Controllers.Admin
{
	public class AdminTextContentController : AdminController
    {
        private PortobelloPostcardsDbContext db = new PortobelloPostcardsDbContext();

        // GET: TextContents
        public ActionResult Index()
        {
            return View(db.TextContent.ToList());
        }

        // GET: TextContents/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TextContent textContent = db.TextContent.Find(id);
            if (textContent == null)
            {
                return HttpNotFound();
            }
            return View(textContent);
        }

		private string TextFromFile(HttpPostedFileBase file)
		{
			string text = string.Empty;

			if(null != file && 0 < file.ContentLength)
			{
				string fileName = Path.GetFileName(file.FileName);
				string path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);

				file.SaveAs(path);

				text = System.IO.File.ReadAllText(path);

				System.IO.File.Delete(path);
			}

			return text;
		}


		[HttpPost]
		public ActionResult Upload([Bind(Include = "Id,Text,Description,Placeholder,MimeType")]TextContent textContent, HttpPostedFileBase file)
		{
			if (null != textContent)
			{
				if (ModelState.IsValid)
				{
					if (null != file)
					{
						textContent.Text = TextFromFile(file);
						textContent.MimeType = file.ContentType;
					}

					TextContent tc = db.TextContent.Where(t => t.Id == textContent.Id).FirstOrDefault();

					if (null == tc)
						db.TextContent.Add(textContent);
					else
					{
						tc.Text = textContent.Text;
						tc.Description = textContent.Description;
						tc.MimeType = textContent.MimeType;
						tc.Placeholder = textContent.Placeholder;

						db.Entry(tc).State = EntityState.Modified;
					}

					db.SaveChanges();
				}
			}

			return RedirectToAction("Index");
		}


        // GET: TextContents/Create
        public ActionResult Create()
        {
            return View();
        }

		////////// POST: TextContents/Create
		////////// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		////////// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//////////[HttpPost]
		//////////[ValidateAntiForgeryToken]
		//////////public ActionResult Create([Bind(Include = "Id,Text,Description,Placeholder,MimeType")] 
		//////////						   TextContent textContent)
		//////////{
		//////////	if (ModelState.IsValid)
		//////////	{
		//////////		db.TextContent.Add(textContent);
		//////////		db.SaveChanges();
		//////////		return RedirectToAction("Index");
		//////////	}

		//////////	return View(textContent);
		//////////}

        // GET: TextContents/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TextContent textContent = db.TextContent.Find(id);
            if (textContent == null)
            {
                return HttpNotFound();
            }
            return View(textContent);
        }

		//////// POST: TextContents/Edit/5
		//////// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//////// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		////////[HttpPost]
		////////[ValidateAntiForgeryToken]
		////////public ActionResult Edit([Bind(Include = "Id,Text,Description,Placeholder,MimeType")] TextContent textContent)
		////////{
		////////	if (ModelState.IsValid)
		////////	{
		////////		db.Entry(textContent).State = EntityState.Modified;
		////////		db.SaveChanges();
		////////		return RedirectToAction("Index");
		////////	}
		////////	return View(textContent);
		////////}

        // GET: TextContents/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TextContent textContent = db.TextContent.Find(id);
            if (textContent == null)
            {
                return HttpNotFound();
            }
            return View(textContent);
        }

        // POST: TextContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TextContent textContent = db.TextContent.Find(id);
            db.TextContent.Remove(textContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
