var _FADE_SPEED = 250;

function makeId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function createSizeableDiv(bid, x, y) {

    var div = $("<div>" +
        "<div class='ui-resizable-handle ui-resizable-nw' id='nwgrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-ne' id='negrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-sw' id='swgrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-se' id='segrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-n' id='ngrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-s' id='sgrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-w' id='wgrip'></div>" +
        "<div class='ui-resizable-handle ui-resizable-e' id='egrip'></div>" +
        "</div>").addClass("bubble-holder").attr({
        id: bid + "_parent",
    }).resizable({
        handles: {
            "nw": "#nwgrip",
            "ne": "#negrip",
            "sw": "#swgrip",
            "se": "#segrip",
            "n": "#ngrip",
            "e": "#egrip",
            "s": "#sgrip",
            "w": "#wgrip"
        },
        autoHide: true
    }).draggable();

    div.css("position", "absolute");
    div.css("left", x);
    div.css("top", y);
    div.css("width", 0);
    div.css("height", 0);
    div.css("z-index", "20");

    div.dblclick(function (e) {

        e.stopPropagation();

        $(this).animate({
            width: 0,
            height: 0
        }, _FADE_SPEED, function () {
            $(this).remove();
        });
    });

    return div
}

function createImage(bid, imgPath) {

    var img = $("<img></img>").attr({
        id: bid,
        src: imgPath
    }).addClass("img-responsive height-responsive bubble");

    img.css("position", "absolute");
    img.css("left", 0);
    img.css("top", 0);
    img.css("width", "100%");
    img.css("height", "100%");
    img.css("z-index", 20);

    return img;
}

function createImageBubble(parent, bid, x, y, w, h, speech) {
 
    var imgPath = null;
    if (speech)
        imgPath = "img/bubbles/speech_l.png";
    else
        imgPath = "img/bubbles/thought_l.png";

    var div = createSizeableDiv(bid, x, y);

    var img = createImage(bid, imgPath);

    div.append(img);

    parent.append(div);
    
    div.show().animate({
        width: w,
        height: h
    }, _FADE_SPEED);
}

function createBubble(parent, cursX, cursY, speech) {

    var w = parent.width() / 4;
    var h = w / 2;

    x = cursX - (w / 2);
    y = cursY - (h / 2);

    createImageBubble(parent, "bubble" + makeId(), x, y, w, h, speech);
}






/*        function loadBackgroundCanvas() {

            var image = new Image();
            image.src = "img/Market.jpg";

            $(image).on("load", function () {

                var canvas = document.getElementById("backgroundCanvas");

                var context = canvas.getContext("2d");

                context.drawImage(image, 0, 0, canvas.width, canvas.height);
            });
        }

        function loadForegroundCanvas() {

            var image = new Image();
            image.src = "img/bubbles/speech_l.png"

            $(image).on("load", function () {

                var canvas = document.getElementById("foregroundCanvas");

                var context = canvas.getContext("2d");

                context.drawImage(image, 0, 0, canvas.width, canvas.height);
            });
        }*/


//
//function createCanvasBubble(parent, id, x, y, speech) {
//
//    var image = new Image();
//    if (speech)
//        image.src = "img/bubbles/speech_l.png";
//    else
//        image.src = "img/bubbles/thought_l.png";
//
//    $(image).on("load", function () {
//
//        //        var h = $("<div>" + 
//        //                  "<div class='ui-resizable-handle ui-resizable-nw' id='nwgrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-ne' id='negrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-sw' id='swgrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-se' id='segrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-n' id='ngrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-s' id='sgrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-w' id='wgrip'></div>" +
//        //                  "<div class='ui-resizable-handle ui-resizable-e' id='egrip'></div>" +
//        //                  "</div>").attr({
//        //            id: hid,
//        //        }).addClass("bubble-canvas").resizable({
//        //            handles: {
//        //                        "nw": "#nwgrip",
//        //                        "ne": "#negrip",
//        //                        "sw": "#swgrip",
//        //                        "se": "#segrip",
//        //                        "n": "#ngrip",
//        //                        "e": "#egrip",
//        //                        "s": "#sgrip",
//        //                        "w": "#wgrip"
//        //                    },
//        //                    autoHide: true
//        //        }).draggable();//.hide();
//
//        //var canvasId = id; //hid + "_canvas";
//        var c = $("<canvas></canvas>").attr({
//            id: id,
//        }).addClass("bubble-canvas"); //.hide();
//
//        //        h.append(c);
//
//        $(parent).append(c);
//
//        var canvas = document.getElementById(canvasId);
//
//        var context = canvas.getContext("2d");
//
//        context.drawImage(image, 0, 0, canvas.width, canvas.height);
//
//        //        h.css("left", x);
//        //        h.css("top", y);
//
//        //        h.show().animate( { left: x, top: y }, 0)
//
//        //c.fadeIn(2000);
//    });
//}