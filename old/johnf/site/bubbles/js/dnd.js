var dragSrcEl = null;

function initSwapDnd(id) {
    
    $(id).on("dragstart", function (e) {
        console.log("dragstart");
        //this.style.opacity = "0.4";
        
        dragSrcEl = this;
        
        e.dataTransfer = e.originalEvent.dataTransfer;
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData("text/html", this.innerHTML);
    });

    $(id).on("dragenter", function (e) {
        console.log("dragenter");
        //$(this).addClass("over");
    });

    $(id).on("dragover", function (e) {
        console.log("dragover");
        if (e.preventDefault) {
            e.preventDefault();
        }
    });

     $(id).on("dragleave", function (e) {
        console.log("dragleave");
        //$(this).removeClass("over");
    });
    
    $(id).on("drop", function (e) {
        console.log("drop");
        if (e.stopPropagation) {
            e.stopPropagation(); // stops the browser from redirecting.
        }
        
        if (dragSrcEl != this) {
            // Set the source column's HTML to the HTML of the column we dropped on.
            dragSrcEl.innerHTML = this.innerHTML;
            e.dataTransfer = e.originalEvent.dataTransfer;
            this.innerHTML = e.dataTransfer.getData('text/html');
        }

        return false;
    });
    
    $(id).on("dragend", function (e) {
        console.log("dragend");
    });
};

function initFileDnd(id) {
    $(id).on("drop", function (evt) {
            evt.stopPropagation();
            evt.preventDefault();

            var files = evt.dataTransfer.files; // FileList object.

            // files is a FileList of File objects. List some properties.
            var output = [];
            for (var i = 0, f; f = files[i]; i++) {
                output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                  f.size, ' bytes, last modified: ',
                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                  '</li>');
            }
        
            //document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
    });
    
    
    $(id).on("change", function (e) {
              e.stopPropagation(); // Stops some browsers from redirecting.
              e.preventDefault();

              var files = e.dataTransfer.files;
              for (var i = 0, f; f = files[i]; i++) {
                if (!f.type.match('image.*')) {
                    continue;
                }

              var reader = new FileReader();

              // Closure to capture the file information.
              reader.onload = (function(theFile) {
                return function(e) {
                  // Render thumbnail.
                  var span = document.createElement('span');
                  span.innerHTML = ['<img class="thumb" src="', e.target.result,
                                    '" title="', escape(theFile.name), '"/>'].join('');
                  //document.getElementById('list').insertBefore(span, null);
                };
              })(f);

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
        
            //document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
    });
};

